<?php
namespace STUBR\SlugL10nHook\Utility;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TypoScriptUtility implements SingletonInterface
{
    /**
     * get the TypoScript settings of EXT:gcsearch
     */
    public static function getSettings(){
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $configurationManager = $objectManager->get(ConfigurationManager::class);
        $typoScriptService = $objectManager->get(TypoScriptService::class);
        $ts = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
        );
        $ts = $typoScriptService->convertTypoScriptArrayToPlainArray($ts);
        return $ts['plugin']['tx_slugl10nhook']['settings'];
    }
}
