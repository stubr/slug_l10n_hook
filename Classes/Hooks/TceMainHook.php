<?php
namespace STUBR\SlugL10nHook\Hooks;

use STUBR\SlugL10nHook\Utility\TypoScriptUtility;

/**
 * Class TceMainHook
 * @package STUBR\SlugL10nHook\Hooks
 */
class TceMainHook {

    /**
     * Reset the slug field of a translated record
     * so the slug field is not derived from the default language
     * (which is the wrong language for a translated record)
     * but instead use the title field to generate the slug filed
     * (or other fields depending on the slug configuration)
     *
     * @param $status
     * @param $table
     * @param $id
     * @param $fieldArray
     * @param $pObj
     */
    function processDatamap_postProcessFieldArray($status, $table, $id, &$fieldArray, &$pObj)
    {
        if ($status !== "new") {
            // only process new records
            return;
        }

        if (!array_key_exists("l10n_parent", $fieldArray)) {
            // current record is in default language
            return;
        }

        if ($fieldArray["l10n_parent"] == 0) {
            // current record is in default language
            return;
        }

        $settings = TypoScriptUtility::getSettings();
        if (!is_array($settings)) {
            // no settings available
            return;
        }
        if (!array_key_exists("recordSlugFieldMapping", $settings)) {
            // no mapping configuration available
            return;
        }

        $recordSlugFieldMapping = $settings["recordSlugFieldMapping"];
        $allowedRecords = array_keys($recordSlugFieldMapping);
        if (in_array($table, $allowedRecords)) {
            $slugField = $recordSlugFieldMapping[$table];
            if (array_key_exists($slugField, $fieldArray)) {
                // reset the slug fields for the translated record
                $fieldArray[$slugField] = "";
            }
        }
    }
}
