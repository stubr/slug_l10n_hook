<?php

$EM_CONF['slug_l10n_hook'] = [
    'title' => 'Slug l10n hook',
    'description' => 'Better UX for handlinng slug fields in translated records',
    'category' => 'misc',
    'author' => 'Patrick Crausaz',
    'author_email' => 'info@sturmundbraem.ch',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
