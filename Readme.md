
# Slug l10n hook

### What does it do
Reset the slug field of a translated record, so the slug field is not derived from the default language record (which is the wrong language for a translated record) but instead use the title field to generate the slug filed, after the records content has been set in the actual language.

### How to use
1. Install the extension
2. Add a TypoScript table-to-slug mapping
```
plugin.tx_slugl10nhook {
  settings {
    recordSlugFieldMapping {
      pages = slug
      tt_content = slug

      # table name => slug field name
      tx_myext_domain_model_entity = path_segment
    }
  }
}
```
